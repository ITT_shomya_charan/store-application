export const products = [
    {
      id: 1,
      name: 'A60',
      price: 799,
      description: 'Fingerprint (rear-mounted), accelerometer, gyro, proximity, compass',
      memory: '64GB 6GB RAM, 128GB 6GB RAM',
      maincamera : '32 MP, f/1.7, 26mm (wide), 1/2.8", 0.8µm, PDAF',
      firstimage : '../assets/images/A60.jpg'
    },
    {
      id: 2,
      name: 'M20',
      price: 699,
      description: 'A great phone with one of the best cameras',
      memory : '32GB 3GB RAM, 64GB 4GB RAM',
      maincamera : '13 MP, f/1.9, 1/3.1", 1.12µm, PDAF',
      firstimage : '../assets/images/M20.jpg'
  
    },
    {
      id: 3,
      name: 'S10',
      price: 299,
      description: 'Fingerprint (under display, ultrasonic), accelerometer, gyro, proximity, compass, barometer, heart rate, SpO2',
      memory :'128GB 8GB RAM, 512GB 8GB RAM',
      maincamera : '12 MP, f/1.5-2.4, 26mm (wide), 1/2.55", 1.4µm, Dual Pixel PDAF, OIS',
      firstimage : '../assets/images/S10.jpg'
    }
  ];
  