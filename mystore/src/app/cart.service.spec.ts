import { TestBed } from '@angular/core/testing';

import { CartService } from './cart.service';

describe('CartService', () => {
  let service: CartService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CartService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

describe('CartService', () => {

  let cartService: CartService;

  let mockHttpClient: any;

  beforeEach(function () {
    cartService = new CartService(mockHttpClient)

  });

  it('Should return shipping data', () => {
    let mockResponse = [
      {
        "type": "Overnight",
        "price": 25.99
      },
      {
        "type": "2-Day",
        "price": 9.99
      },
      {
        "type": "Postal",
        "price": 2.99
      }
    ];

    let response: any;
    spyOn(cartService, 'getShippingPrices').and.returnValue(of(mockResponse));
    cartService.getShippingPrices().subscribe(res => { response = res });
    expect(response).toEqual(mockResponse)



  });
})